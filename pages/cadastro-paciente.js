import { useState, useEffect } from 'react'
import Link from "next/link";
import { useRouter } from "next/router";
import { LockClosedIcon } from "@heroicons/react/solid";
import NumberFormat from 'react-number-format';

export default function Example() {
  const [registerError, setRegisterError] = useState(null)

  const [nome, setNome] = useState('')
  const [cpf, setCpf] = useState('')
  const [senha, setSenha] = useState('')
  const [resenha, setReSenha] = useState('')
  const [anamneseOne, setAnamneseOne] = useState('')
  const [anamneseTwo, setAnamneseTwo] = useState('')
  const [anamneseThree, setAnamneseThree] = useState('')
  const [anamneseFour, setAnamneseFour] = useState('')

  const router = useRouter()

  const submitData = async (e) => {
    e.preventDefault()
    try {
      const body = { nome, cpf, senha, anamneseOne, anamneseTwo, anamneseThree, anamneseFour }
      await fetch('/api/paciente/register', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(body),
      })
      if (typeof window !== "undefined") {
        localStorage.setItem("@paciente", cpf)
        router.push('/paciente')
      }
    } catch (error) {
      console.error(error)
      setRegisterError('erro no cadastro, verifique os dados e tente novamente!')
    }
  }

  function checkAuth() {
    if (typeof window !== "undefined") {
      const logged = localStorage.getItem("@paciente")

      if (!!logged) {
        router.push('/paciente')
      }      
    }
  }

  useEffect(() => {
    checkAuth()
  }, [])
  return (
    <div className="min-h-screen flex items-center justify-center bg-gray-50 py-12 px-4 sm:px-6 lg:px-8">
      <div className="max-w-md w-full space-y-8">
        <div>
          <h2 className="mt-6 text-center text-6xl font-extrabold text-gray-900">
            Telespirômetro
          </h2>
          <p className="mt-2 text-center text-2xl text-gray-600">
            Cadastro de paciente
          </p>
        </div>
        <form className="mt-8 space-y-4" onSubmit={e => submitData(e)}>
          <input type="hidden" name="remember" defaultValue="true" />
          <div className="rounded-md shadow-sm space-y-2">
            <div className="py-2">
              <label htmlFor="fullname" className="sr-only">
                Nome completo
              </label>
              <input
                id="fullname"
                name="fullname"
                type="text"
                autoComplete="fullname"
                value={nome}
                onChange={e => setNome(e.target.value)}
                required
                className="rounded-md block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                placeholder="Nome completo"
              />
            </div>
            <div className="py-2">
              <label htmlFor="cpf" className="sr-only">
                CPF
              </label>
              <NumberFormat
                id="cpf"
                name="cpf"
                value={cpf}
                onChange={e => setCpf(e.target.value)}
                autoComplete="CPF"
                required
                className="rounded-md block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                placeholder="CPF"
                format="###.###.###-##"
              />
            </div>
            <div className="py-2">
              <label htmlFor="password" className="sr-only">
                Senha
              </label>
              <input
                id="password"
                name="password"
                type="password"
                value={senha}
                onChange={e => setSenha(e.target.value)}
                minLength={6}
                autoComplete="current-password"
                required
                className="block w-full rounded-md px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                placeholder="Senha"
              />
            </div>
            <div className="py-2">
              <label htmlFor="repassword" className="sr-only">
                Confirmar Senha
              </label>
              <input
                id="repassword"
                name="repassword"
                type="password"
                value={resenha}
                onChange={e => setReSenha(e.target.value)}
                autoComplete="current-repassword"
                required
                className="block w-full rounded-md px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                placeholder="Confirmar senha"
              />
            </div>
          </div>

          <div className="rounded-md shadow-sm">
            <h2 className="text-xl text-bold text-gray-700">Anamnese</h2>
            <div className="py-2">
              <label htmlFor="anamnese-one" className="text-sm text-gray-600">
                Por que procurou atendimento médico?
              </label>
              <input
                id="anamnese-one"
                name="anamnese-one"
                type="text"
                value={anamneseOne}
                onChange={e => setAnamneseOne(e.target.value)}
                autoComplete="anamnese-one"
                required
                className="rounded-md block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                placeholder="Digite a resposta nesse campo"
              />
            </div>
            <div className="py-2">
              <label htmlFor="anamnese-two" className="text-sm text-gray-600">
                Já esteve contaminado com covid 19? Se sim, há quanto tempo?
              </label>
              <input
                id="anamnese-two"
                name="anamnese-two"
                type="text"
                value={anamneseTwo}
                onChange={e => setAnamneseTwo(e.target.value)}
                autoComplete="anamnese-two"
                required
                className="rounded-md block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                placeholder="Digite a resposta nesse campo"
              />
            </div>
            <div className="py-2">
              <label htmlFor="anamnese-three" className="text-sm text-gray-600">
                Possui asma, bronquite, rinite alérgica ou conjuntivite
                alérgica? Se sim, quais?
              </label>
              <input
                id="anamnese-three"
                name="anamnese-three"
                type="text"
                value={anamneseThree}
                onChange={e => setAnamneseThree(e.target.value)}
                autoComplete="anamnese-three"
                required
                className="rounded-md block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                placeholder="Digite a resposta nesse campo"
              />
            </div>
            <div className="py-2">
              <label htmlFor="anamnese-four" className="text-sm text-gray-600">
                Você é fumante? Passivo ou ativo?
              </label>
              <input
                id="anamnese-four"
                name="anamnese-four"
                type="text"
                value={anamneseFour}
                onChange={e => setAnamneseFour(e.target.value)}
                autoComplete="anamnese-four"
                required
                className="rounded-md block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                placeholder="Digite a resposta nesse campo"
              />
            </div>
          </div>
          
          {senha !== resenha && (
            <p className="px-4 py-2 bg-red-100 text-red-500 rounded text-center">As senhas devem ser iguais</p>
          )}
          {!!registerError && (
            <p className="px-4 py-2 bg-red-100 text-red-500 rounded text-center">{registerError}</p>
          )}

          <div>
            <button
              type="submit"
              disabled={senha !== resenha || !senha}
              className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            >
              <span className="absolute left-0 inset-y-0 flex items-center pl-3">
                <LockClosedIcon
                  className="h-5 w-5 text-indigo-500 group-hover:text-indigo-400"
                  aria-hidden="true"
                />
              </span>
              Cadastrar paciente
            </button>
          </div>
          <div>
            <Link href="/login-paciente">
              <button
                type="button"
                className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md bg-gray-100 text-indigo-600  hover:bg-gray-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              >
                Sou cadastrado
              </button>
            </Link>
          </div>
          <div className="text-sm">
            <Link href="/">
              <a
                href="#"
                className="font-medium text-indigo-600 hover:text-indigo-500"
              >
                Voltar a tela inicial
              </a>
            </Link>
          </div>
        </form>
      </div>
    </div>
  );
}
