import prisma from '../../../lib/prisma'

export default async function handle(req, res) {
  const { cpf, senha } = req.body

  const result = await prisma.paciente.findUnique({
    where: {
      cpf
    }
  })

  if (result) {
    res.status(200).json(result)
  } else {
    res.status(400).json({ error: "user not found" })
  }
}