import prisma from '../../../lib/prisma'

// POST /api/post
// Required fields in body: title
// Optional fields in body: content
export default async function handle(req, res) {
  const { nome, cpf, senha, anamneseOne, anamneseTwo, anamneseThree, anamneseFour  } = req.body

  const result = await prisma.paciente.create({
    data: {
      nome, cpf, senha, anamneseOne, anamneseTwo, anamneseThree, anamneseFour
    },
  })
  res.json(result)
}