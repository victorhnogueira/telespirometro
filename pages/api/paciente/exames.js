import prisma from '../../../lib/prisma'

// POST /api/post
// Required fields in body: title
// Optional fields in body: content
export default async function handle(req, res) {
  const { pacienteCpf } = req.body

  const paciente = await prisma.paciente.findUnique({
    where: {
      cpf: pacienteCpf
    }
  })

  if (!paciente) {
    res.status(400).json({ error: "paciente not found" })
  } else {
    const result = await prisma.exame.findMany({
        where: {
            pacienteId: Number(paciente.id)
        },
        include: {
            paciente: {}
        }
    })
    res.status(200).json(result)
  }
}