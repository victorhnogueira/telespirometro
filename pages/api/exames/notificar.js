import prisma from '../../../lib/prisma'

// POST /api/post
// Required fields in body: title
// Optional fields in body: content
export default async function handle(req, res) {
  const { exameId, profissionalCpf, mensagem  } = req.body

  const exame = await prisma.exame.findUnique({
    where: {
      id: Number(exameId)
    }
  })

  const profissional = await prisma.profissional.findUnique({
    where: {
      cpf: profissionalCpf
    }
  })

  if (!exame) {
    res.status(400).json({ error: "exame not found" })
  } else if (!profissional) {
    res.status(400).json({ error: "profissional not found" })
  }else {
    const result = await prisma.notificacao.create({
      data: {
        mensagem,
        exameId: exame.id,
        profissionalId: Number(profissional.id),
      },
    })
    res.status(200).json(result)
  }
}