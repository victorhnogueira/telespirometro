import prisma from '../../../lib/prisma'

// POST /api/post
// Required fields in body: title
// Optional fields in body: content
export default async function handle(req, res) {
  const { cpf, tempo, dados, tempoTwo, dadosTwo } = req.body

  const paciente = await prisma.paciente.findUnique({
    where: {
      cpf
    }
  })

  if (!paciente) {
    res.status(400).json({ error: "user not found" })
  } else {
    const result = await prisma.exame.create({
      data: {
        tempo,
        dados,
        tempoTwo,
        dadosTwo,
        pacienteId: paciente.id
      },
    })
    res.status(200).json(result)
  }
}