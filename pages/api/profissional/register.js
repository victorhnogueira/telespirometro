import prisma from '../../../lib/prisma'

// POST /api/post
// Required fields in body: title
// Optional fields in body: content
export default async function handle(req, res) {
  const { nome, cpf, conselho, senha  } = req.body

  const result = await prisma.profissional.create({
    data: {
        nome, cpf, conselho, senha
    },
  })
  res.json(result)
}