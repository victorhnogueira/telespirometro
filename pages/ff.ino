#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>

String cpf;

// Variaveis de conxão com a rede WiFi local
const char * ssid = "FDAG";
const char * password = "A1B2C3D4";

// Definição dos dados que serão enviados via POST
const char * segundos = "[0,0.5,3,5,6,7]";
const char * leitura = "[0,7,4,0.5,0,0]";
const char * segundosTwo = "[0,1,3,4,5,6,7,8,9,10,11,12,13,14,15]";
const char * leituraTwo = "[0,6,7,7,7,7,7,7,7,7,7,7,7,7,7]";

void setup() {
  Serial.begin(115200);
  // Delay antes de chamar a funcao WiFi.begin
  delay(4000);

  WiFi.begin(ssid, password);

  // Verifica o status da conexão com a rede WiFi
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Conectando a rede WiFi...");
  }

  Serial.println("Conectado com sucesso na rede");

}

void loop() {
  // Verifica o status da conexão com a rede WiFi
  if (WiFi.status() == WL_CONNECTED) {

    //se byte pronto para leitura
    if (Serial.available() > 0) {
      // Armazena CPF digitado para enviar via POST
      cpf = leStringSerial();

      enviaDadosEspirometro(cpf);
    }
  } else {
    Serial.println("Erro ao conectar a rede WiFi");
  }
  delay(10000); // Aguarda 10 segundos para uma nova requisição
}

// Função responsável por fazer as requisições HTTP
void enviaDadosEspirometro(String cpf) {
  HTTPClient http;

  // Define a URL para onde serão feitas as requisições
  http.begin("http://teleespirometro.herokuapp.com/api/exames/create");
  // Define o tipo de dado enviado para JSON
  http.addHeader("Content-Type", "application/json");
  // Construção do JSON com as informações enviadas
  int httpResponseCode = http.POST("{\"cpf\": \"" + String(cpf) + "\",\"tempo\": \"" + String(segundos) + "\",\"dados\": \"" + String(leitura) + "\", \"tempoTwo\":\"" + String(segundosTwo) + "\",\"dadosTwo\": \"" + String(leituraTwo) + "\"}");

  if (httpResponseCode > 0) {

    String response = http.getString(); // salva o retorno da requisição

    Serial.println(httpResponseCode); // Printa o código de retorno
    Serial.println(response); // Printa a resposta do servidor

  } else {

    Serial.print("Erro ao enviar requisição POST: ");
    Serial.println(httpResponseCode);
  }

  http.end(); // Libera para novas requisições
}

// Função para ler todo o CPF digitado
String leStringSerial() {
  String conteudo = "";
  char caractere;

  // Enquanto estiver recebendo algo pelo serial
  while (Serial.available() > 0) {
    // Lê byte da serial
    caractere = Serial.read();
    // Ignora caractere de quebra de linha
    if (caractere != '\n') {
      // Concatena valores
      conteudo.concat(caractere);
    }
    // Aguarda buffer serial ler próximo caractere
    delay(10);
  }

  return conteudo;
}