import { useEffect, useState } from 'react'
import Link from "next/link";
import { useRouter } from "next/router";
import { LockClosedIcon } from "@heroicons/react/solid";

export default function Example() {
  const [loginError, setLoginError] = useState(null)
  const [cpf, setCpf] = useState('')
  const [senha, setSenha] = useState('')

  const router = useRouter()

  const submitData = async (e) => {
    e.preventDefault()
    try {
      const body = { cpf, senha }
      await fetch('/api/paciente/login', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(body),
      }).then(response => {
        response.json().then(function(data) {
          const { cpf } = data
          if (!!cpf) {
            if (typeof window !== "undefined") {
              localStorage.setItem("@paciente", cpf)
              router.push('/paciente')
            }
          } else {
            setLoginError("Dados inválidos")
          }
        });
        
      })
      
    } catch (error) {
      console.error(error)
      setRegisterError('erro no login, verifique os dados e tente novamente!')
    }
  }

  function checkAuth() {
    if (typeof window !== "undefined") {
      const logged = localStorage.getItem("@paciente")

      if (!!logged) {
        router.push('/paciente')
      }      
    }
  }

  useEffect(() => {
    checkAuth()
  }, [])
  return (
    <div className="min-h-screen flex items-center justify-center bg-gray-50 py-12 px-4 sm:px-6 lg:px-8">
      <div className="max-w-md w-full space-y-8">
        <div>
          <h2 className="mt-6 text-center text-6xl font-extrabold text-gray-900">
            Telespirômetro
          </h2>
          <p className="mt-2 text-center text-2xl text-gray-600">
            Entrar como paciente
          </p>
        </div>
        <form className="mt-8 space-y-6" onSubmit={e => submitData(e)}>
          <input type="hidden" name="remember" defaultValue="true" />
          <div className="rounded-md shadow-sm -space-y-px">
            <div>
              <label htmlFor="cpf" className="sr-only">
                CPF
              </label>
              <input
                id="cpf"
                name="cpf"
                type="text"
                value={cpf}
                onChange={e => setCpf(e.target.value)}
                autoComplete="CPF"
                required
                className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                placeholder="CPF"
              />
            </div>
            <div>
              <label htmlFor="password" className="sr-only">
                Senha
              </label>
              <input
                id="password"
                name="password"
                type="password"
                value={senha}
                onChange={e => setSenha(e.target.value)}
                autoComplete="current-password"
                required
                className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                placeholder="******"
              />
            </div>
          </div>

          {!!loginError && (
            <p className="px-4 py-2 bg-red-100 text-red-500 rounded text-center">{loginError}</p>
          )}

          <div>
              <button
              type="submit"
              className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              >
              <span className="absolute left-0 inset-y-0 flex items-center pl-3">
                  <LockClosedIcon
                  className="h-5 w-5 text-indigo-500 group-hover:text-indigo-400"
                  aria-hidden="true"
                  />
              </span>
              Entrar
              </button>
          </div>

          <div>
            <Link href="/cadastro-paciente">
              <button
                type="button"
                className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md bg-gray-100 text-indigo-600  hover:bg-gray-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              >
                Cadastro
              </button>
            </Link>
          </div>
          <div className="text-sm">
            <Link href="/">
              <a
                href="#"
                className="font-medium text-indigo-600 hover:text-indigo-500"
              >
                Voltar a tela inicial
              </a>
            </Link>
          </div>
        </form>
      </div>
    </div>
  );
}
