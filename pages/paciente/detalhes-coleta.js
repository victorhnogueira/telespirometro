import Link from "next/link";
import { Fragment } from "react";
import { Disclosure, Menu, Transition } from "@headlessui/react";
import { BellIcon, MenuIcon, XIcon } from "@heroicons/react/outline";
import { PaperClipIcon } from "@heroicons/react/solid";
import { Line } from "react-chartjs-2";

const data = {
  labels: ["0", "0.5", "3", "5", "6", "7"],
  datasets: [
    {
      label: "Pressão",
      data: [0, 7, 4, 0.5, 0, 0],
      fill: false,
      backgroundColor: "rgb(40, 150, 114)",
      borderColor: "rgba(40, 150, 114, 0.8)",
      lineTension: 0.4,
      radius: 0,
    },
  ],
};

const dataTwo = {
  labels: [
    "0",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "10",
    "11",
    "12",
    "13",
    "14",
    "15",
  ],
  datasets: [
    {
      label: "Pressão",
      data: [0, 6, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7],
      fill: false,
      backgroundColor: "rgb(255, 99, 132)",
      borderColor: "rgba(255, 99, 132, 0.8)",
      lineTension: 0.4,
      radius: 0,
    },
  ],
};

const options = {
  plugins: {
    legend: {
      display: false,
    },
  },
  scales: {
    y: {
      ticks: {
        // forces step size to be 50 units
        stepSize: 2,
      },
      title: {
        display: true,
        text: "Fluxo (L/s)",
        font: {
          size: 20,
        },
      },
    },
    x: {
      title: {
        display: true,
        text: "Volume(L)",
        font: {
          size: 20,
        },
      },
    },
  },
};

const optionsTwo = {
  plugins: {
    legend: {
      display: false,
    },
  },
  scales: {
    y: {
      ticks: {
        // forces step size to be 50 units
        stepSize: 2,
      },
      title: {
        display: true,
        text: "Volume (L)",
        font: {
          size: 20,
        },
      },
    },
    x: {
      title: {
        display: true,
        text: "Tempo(S)",
        font: {
          size: 20,
        },
      },
    },
  },
};

const navigation = [
  { name: "Meus exames", href: "/paciente", current: true },
  { name: "Realizar exame", href: "/paciente/coleta", current: false },
];

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function Example() {
  return (
    <>
      <Disclosure as="nav" className="bg-gray-800">
        {({ open }) => (
          <>
            <div className="max-w-7xl mx-auto px-2 sm:px-6 lg:px-8">
              <div className="relative flex items-center justify-between h-16">
                <div className="absolute inset-y-0 left-0 flex items-center sm:hidden">
                  {/* Mobile menu button*/}
                  <Disclosure.Button className="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white">
                    <span className="sr-only">Abrir menu</span>
                    {open ? (
                      <XIcon className="block h-6 w-6" aria-hidden="true" />
                    ) : (
                      <MenuIcon className="block h-6 w-6" aria-hidden="true" />
                    )}
                  </Disclosure.Button>
                </div>
                <div className="flex-1 flex items-center justify-center sm:items-stretch sm:justify-start">
                  <div className="flex-shrink-0 flex items-center">
                    <Link href="/paciente">
                      <h1 className="text-white cursor-pointer">
                        Telespirômetro
                      </h1>
                    </Link>
                  </div>
                  <div className="hidden sm:block sm:ml-6">
                    <div className="flex space-x-4">
                      {navigation.map((item) => (
                        <a
                          key={item.name}
                          href={item.href}
                          className={classNames(
                            item.current
                              ? "bg-gray-900 text-white"
                              : "text-gray-300 hover:bg-gray-700 hover:text-white",
                            "px-3 py-2 rounded-md text-sm font-medium"
                          )}
                          aria-current={item.current ? "page" : undefined}
                        >
                          {item.name}
                        </a>
                      ))}
                    </div>
                  </div>
                </div>
                <div className="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
                  <button className="bg-gray-800 p-1 rounded-full text-gray-400 hover:text-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white">
                    <span className="sr-only">Ver notificações</span>
                    <BellIcon className="h-6 w-6" aria-hidden="true" />
                  </button>

                  {/* Profile dropdown */}
                  <Menu as="div" className="ml-3 relative">
                    {({ open }) => (
                      <>
                        <div>
                          <Menu.Button className="bg-gray-800 flex text-sm rounded-full focus:outline-none">
                            <span className="sr-only">Open user menu</span>
                            <p className="text-white rounded-full px-3 py-2">
                              paciente
                            </p>
                          </Menu.Button>
                        </div>
                        <Transition
                          show={open}
                          as={Fragment}
                          enter="transition ease-out duration-100"
                          enterFrom="transform opacity-0 scale-95"
                          enterTo="transform opacity-100 scale-100"
                          leave="transition ease-in duration-75"
                          leaveFrom="transform opacity-100 scale-100"
                          leaveTo="transform opacity-0 scale-95"
                        >
                          <Menu.Items
                            static
                            className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none"
                          >
                            <Menu.Item>
                              {({ active }) => (
                                <Link href="/">
                                  <a
                                    href="#"
                                    className={classNames(
                                      active ? "bg-gray-100" : "",
                                      "block px-4 py-2 text-sm text-gray-700"
                                    )}
                                  >
                                    Sair
                                  </a>
                                </Link>
                              )}
                            </Menu.Item>
                          </Menu.Items>
                        </Transition>
                      </>
                    )}
                  </Menu>
                </div>
              </div>
            </div>

            <Disclosure.Panel className="sm:hidden">
              <div className="px-2 pt-2 pb-3 space-y-1">
                {navigation.map((item) => (
                  <a
                    key={item.name}
                    href={item.href}
                    className={classNames(
                      item.current
                        ? "bg-gray-900 text-white"
                        : "text-gray-300 hover:bg-gray-700 hover:text-white",
                      "block px-3 py-2 rounded-md text-base font-medium"
                    )}
                    aria-current={item.current ? "page" : undefined}
                  >
                    {item.name}
                  </a>
                ))}
              </div>
            </Disclosure.Panel>
          </>
        )}
      </Disclosure>
      <div className="container mx-auto px-2 sm:px-10 py-4">
        <div className="p-2 sm:p-5">
          <h2 className="mt-6 text-5xl font-extrabold text-gray-900">
            Detalhes do exame
          </h2>
          <p className="mt-2 text-xl text-gray-600">Paciente: Arthur Saraiva</p>
        </div>
        <div className="grid grid-cols-1 md:grid-cols-2 gap-2 mt-14">
          <div>
            <Line data={data} options={options} />
          </div>
          <div>
            <Line data={dataTwo} options={optionsTwo} />
          </div>
        </div>
      </div>
      <div className="container mx-auto px-2 sm:px-10 py-4">
        <div className="bg-white shadow overflow-hidden sm:rounded-lg max-w-lg">
          <div className="px-4 py-5 sm:px-6">
            <h3 className="text-lg leading-6 font-medium text-gray-900">
              Feedback do exame
            </h3>
            <p className="mt-1 max-w-2xl text-sm text-gray-500">
              detalhes enviados pelo profissional da saúde
            </p>
          </div>
          <div className="border-t border-gray-200">
            <dl>
              <div className="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                <dt className="text-sm font-medium text-gray-500">
                  Profissional
                </dt>
                <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                  Dr. Matheus Martins
                </dd>
              </div>
              <div className="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                <dt className="text-sm font-medium text-gray-500">
                  Data do feedback
                </dt>
                <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                  06/06/2021 - 14:56:12
                </dd>
              </div>
              <div className="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                <dt className="text-sm font-medium text-gray-500">Feedback</dt>
                <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                  Com base nos dados analisados é possivel constatar que o
                  paciente necessita iniciar o uso de tal medicamento para um
                  tratamento adequado.
                </dd>
              </div>
            </dl>
          </div>
        </div>
      </div>
    </>
  );
}
