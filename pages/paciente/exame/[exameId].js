import Link from "next/link";
import { useRouter } from "next/router";
import { Fragment, useState, useEffect } from "react";
import { Disclosure, Menu, Transition } from "@headlessui/react";
import { BellIcon, MenuIcon, XIcon } from "@heroicons/react/outline";
import prisma from '../../../lib/prisma'
import { Line } from "react-chartjs-2";
import { format } from 'date-fns'
  
  const options = {
    animation: false,
    plugins: {
      legend: {
        display: false,
      },
    },
    scales: {
      y: {
        ticks: {
          // forces step size to be 50 units
          stepSize: 2,
        },
        title: {
          display: true,
          text: "Fluxo (L/s)",
          font: {
            size: 20,
          },
        },
      },
      x: {
        title: {
          display: true,
          text: "Volume(L)",
          font: {
            size: 20,
          },
        },
      },
    },
  };
  
  const optionsTwo = {
    animation: false,
    plugins: {
      legend: {
        display: false,
      },
    },
    scales: {
      y: {
        ticks: {
          // forces step size to be 50 units
          stepSize: 2,
        },
        title: {
          display: true,
          text: "Volume (L)",
          font: {
            size: 20,
          },
        },
      },
      x: {
        title: {
          display: true,
          text: "Tempo(S)",
          font: {
            size: 20,
          },
        },
      },
    },
  };

const navigation = [
    { name: "Meus exames", href: "/paciente", current: false },
    { name: "Realizar exame", href: "/paciente/coleta", current: false }
];

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function Example({ exame }) {
  const CV = Math.max.apply(null, JSON.parse(exame.dadosTwo))
  const FEV1_IN = JSON.parse(exame.tempoTwo).findIndex(el => el === 1)
  const FEV1 = JSON.parse(exame.dadosTwo)[FEV1_IN]
  const RAZ = Number((FEV1/CV) * 100).toFixed(2)

  const data = {
    labels: JSON.parse(exame.tempo),
    datasets: [
      {
        label: "Pressão",
        data: JSON.parse(exame.dados),
        fill: false,
        backgroundColor: "rgb(40, 150, 114)",
        borderColor: "rgba(40, 150, 114, 0.8)",
        lineTension: 0.4,
        radius: 0,
      },
    ],
  };
  
  const dataTwo = {
    labels: JSON.parse(exame.tempoTwo),
    datasets: [
      {
        label: "Pressão",
        data: JSON.parse(exame.dadosTwo),
        fill: false,
        backgroundColor: "rgb(255, 99, 132)",
        borderColor: "rgba(255, 99, 132, 0.8)",
        lineTension: 0.4,
        radius: 0,
      },
    ],
  };

  const router = useRouter()

  function logout() {
    if (typeof window !== "undefined") {
      localStorage.removeItem("@paciente")
      router.push('/login-paciente')
    }
  }

  function checkAuth() {
    if (typeof window !== "undefined") {
      const logged = localStorage.getItem("@paciente")

      if (!logged) {
        router.push('/login-paciente')
      }      
    }
  }

  useEffect(() => {
    checkAuth()
  }, [])

  return (
    <>
      <Disclosure as="nav" className="bg-gray-800">
        {({ open }) => (
          <>
            <div className="max-w-7xl mx-auto px-2 sm:px-6 lg:px-8">
              <div className="relative flex items-center justify-between h-16">
                <div className="absolute inset-y-0 left-0 flex items-center sm:hidden">
                  {/* Mobile menu button*/}
                  <Disclosure.Button className="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white">
                    <span className="sr-only">Abrir menu</span>
                    {open ? (
                      <XIcon className="block h-6 w-6" aria-hidden="true" />
                    ) : (
                      <MenuIcon className="block h-6 w-6" aria-hidden="true" />
                    )}
                  </Disclosure.Button>
                </div>
                <div className="flex-1 flex items-center justify-center sm:items-stretch sm:justify-start">
                  <div className="flex-shrink-0 flex items-center">
                    <Link href="/profissional">
                      <h1 className="text-white cursor-pointer">
                        Telespirômetro
                      </h1>
                    </Link>
                  </div>
                  <div className="hidden sm:block sm:ml-6">
                    <div className="flex space-x-4">
                      {navigation.map((item) => (
                        <a
                          key={item.name}
                          href={item.href}
                          className={classNames(
                            item.current
                              ? "bg-gray-900 text-white"
                              : "text-gray-300 hover:bg-gray-700 hover:text-white",
                            "px-3 py-2 rounded-md text-sm font-medium"
                          )}
                          aria-current={item.current ? "page" : undefined}
                        >
                          {item.name}
                        </a>
                      ))}
                    </div>
                  </div>
                </div>
                <div className="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
                  <button className="bg-gray-800 p-1 rounded-full text-gray-400 hover:text-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white">
                    <span className="sr-only">Ver notificações</span>
                    <BellIcon className="h-6 w-6" aria-hidden="true" />
                  </button>

                  {/* Profile dropdown */}
                  <Menu as="div" className="ml-3 relative">
                    {({ open }) => (
                      <>
                        <div>
                          <Menu.Button className="bg-gray-800 flex text-sm rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white">
                            <span className="sr-only">Open user menu</span>
                            <p className="text-white rounded-full px-3 py-2">
                              paciente
                            </p>
                          </Menu.Button>
                        </div>
                        <Transition
                          show={open}
                          as={Fragment}
                          enter="transition ease-out duration-100"
                          enterFrom="transform opacity-0 scale-95"
                          enterTo="transform opacity-100 scale-100"
                          leave="transition ease-in duration-75"
                          leaveFrom="transform opacity-100 scale-100"
                          leaveTo="transform opacity-0 scale-95"
                        >
                          <Menu.Items
                            static
                            className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none"
                          >
                            <Menu.Item>
                              {({ active }) => (
                                  <button
                                    onClick={() => logout()}
                                    className={classNames(
                                      active ? "bg-gray-100" : "",
                                      "w-full text-left px-4 py-2 text-sm text-gray-700"
                                    )}
                                  >
                                    Sair
                                  </button>
                              )}
                            </Menu.Item>
                          </Menu.Items>
                        </Transition>
                      </>
                    )}
                  </Menu>
                </div>
              </div>
            </div>

            <Disclosure.Panel className="sm:hidden">
              <div className="px-2 pt-2 pb-3 space-y-1">
                {navigation.map((item) => (
                  <a
                    key={item.name}
                    href={item.href}
                    className={classNames(
                      item.current
                        ? "bg-gray-900 text-white"
                        : "text-gray-300 hover:bg-gray-700 hover:text-white",
                      "block px-3 py-2 rounded-md text-base font-medium"
                    )}
                    aria-current={item.current ? "page" : undefined}
                  >
                    {item.name}
                  </a>
                ))}
              </div>
            </Disclosure.Panel>
          </>
        )}
      </Disclosure>
      <div className="container mx-auto px-10 py-4">
        <div className="px-5">
          <h2 className="mt-6 text-5xl font-extrabold text-gray-900">
            Detalhes do exame
          </h2>
          <p className="mt-2 text-xl text-gray-600">Paciente: {exame?.paciente?.nome}</p>
        </div>
        <div className="grid grid-cols-1 md:grid-cols-2 gap-2 mt-14">
          <div>
            <Line data={data} options={options} />
          </div>
          <div>
            <Line data={dataTwo} options={optionsTwo} />
          </div>
        </div>
      </div>
      <div className="container mx-auto px-10 sm:px-14 py-4">
        <div className="px-5">
          <h2 className="mt-6 text-2xl font-extrabold text-gray-700">
            Metricas do exame
          </h2>
        </div>
        <div className="grid grid-cols-1 md:grid-cols-3 gap-2 my-6  max-w-lg ">
          <div className="border-2 border-gray-400 bg-gray-50 rounded-md px-4 py-2">
            <p className="mt-2 text-xl text-gray-600">FVC</p>
            <p className="mt-2 text-3xl text-gray-600">
              {CV} <small>[L/s]</small>
            </p>
          </div>
          <div className="border-2 border-gray-400 bg-gray-50 rounded-md px-4 py-2">
            <p className="mt-2 text-xl text-gray-600">FEV<small>1</small></p>
            <p className="mt-2 text-3xl text-gray-600">
              {FEV1} <small>[L/s]</small>
            </p>
          </div>
          <div className="border-2 border-gray-400 bg-gray-50 rounded-md px-4 py-2">
            <p className="mt-2 text-xl text-gray-600">FVC/FEV<small>1</small></p>
            <p className="mt-2 text-3xl text-gray-600">{RAZ}%</p>
          </div>
        </div>
      </div>
      {exame.Notificacao.length !==0 && exame.Notificacao.map(notificacao => (
        <div className="container mx-auto px-2 sm:px-10 py-4" key={notificacao.id}>
        <div className="bg-white shadow overflow-hidden sm:rounded-lg max-w-lg">
          <div className="px-4 py-5 sm:px-6">
            <h3 className="text-lg leading-6 font-medium text-gray-900">
              Feedback do exame
            </h3>
            <p className="mt-1 max-w-2xl text-sm text-gray-500">
              detalhes enviados pelo profissional da saúde
            </p>
          </div>
          <div className="border-t border-gray-200">
            <dl>
              <div className="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                <dt className="text-sm font-medium text-gray-500">
                  Profissional
                </dt>
                <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                  {notificacao.profissional?.nome}
                </dd>
              </div>
              <div className="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                <dt className="text-sm font-medium text-gray-500">
                  Data do feedback
                </dt>
                <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                  {format(new Date(notificacao.createdAt), 'dd/MM/yyyy HH:mm:ss')}
                </dd>
              </div>
              <div className="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                <dt className="text-sm font-medium text-gray-500">Feedback</dt>
                <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                  {notificacao.mensagem}
                </dd>
              </div>
            </dl>
          </div>
        </div>
      </div>
      ))}
    </>
  );
}

export const getStaticPaths = async () => {

  return {
      paths: [], //indicates that no page needs be created at build time
      fallback: 'blocking' //indicates the type of fallback
  }
}

export const getStaticProps = async (context) => {
    const { exameId } = context.params

  const exame = await prisma.exame.findUnique({
    where: {
      id: Number(exameId),
    },
    include: {
      Notificacao: {
        include: {
          profissional: {
            select: { nome: true }
          }
        }
      },
      paciente: {
        select: { nome: true }
      }
    }
  })

  if (!exame) {
    return {
      redirect: {
        destination: "/paciente",
      },
    }
  }

  return {props: { exame } }
}