import Head from 'next/head'
import Link from 'next/link'

export default function Home() {
  return (
    <div className="flex flex-col items-center justify-center min-h-screen py-2">
      <Head>
        <title>Telespirometro</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className="flex flex-col items-center justify-center w-full flex-1 px-20 text-center">
        <h1 className="text-5xl  font-bold">
          Telespirômetro
        </h1>

        <p className="mt-3 text-2xl">
          escolha como deseja continuar:
        </p>

        <div className="flex flex-wrap items-center justify-around max-w-4xl mt-6 sm:w-full">
          <Link href="/login-paciente">
            <a
                href="/login-paciente"
                className="p-6 mt-6 text-left border w-96 rounded-xl hover:text-blue-600 focus:text-blue-600"
            >
                <h3 className="text-2xl font-bold">Paciente &rarr;</h3>
                <p className="mt-4 text-xl">
                Visualizar exames, dados pessoais, etc.
                </p>
            </a>
          </Link>
          <Link href="/login-profissional">
          <a
            href="/login-profissional"
            className="p-6 mt-6 text-left border w-96 rounded-xl hover:text-blue-600 focus:text-blue-600"
          >
            <h3 className="text-2xl font-bold">Profissional da saúde &rarr;</h3>
            <p className="mt-4 text-xl">
              Realizar novas consultas, ver pacientes cadastrados, etc.
            </p>
          </a>
          </Link>
        </div>
      </main>

      <footer className="flex items-center justify-center w-full h-24 border-t px-4">
        <p
          className="flex items-center justify-center"
        >
          Arthur Henrique Saraiva, João Marcus Benedito, Matheus Martins.
        </p>
      </footer>
    </div>
  )
}
